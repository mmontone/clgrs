(asdf:defsystem #:clgrs
  :description "Common Lisp Grammar Services"
  :author "Mariano Montone <marianomontone@gmail.com>"
  :license  "MIT"
  :version "0.0.1"
  :serial t
  :components ((:module "src"
                        :components
                        ((:file "package")
                         (:file "clgrs")
                         (:file "rules")
                         (:file "print")
                         (:file "parser")
                         (:file "pretty")
                         (:file "assist")
                         (:file "simple-snippet")
                         (:file "doc")
                         (:file "refactor")
                         (:file "defstruct")
                         (:file "defpackage"))))
  :depends-on (:alexandria :str :closer-mop)
  :in-order-to ((asdf:test-op (asdf:test-op :clgrs.test))))
