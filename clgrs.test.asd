(asdf:defsystem #:clgrs.test
  :description "Common Lisp Grammar Services tests"
  :author "Mariano Montone <marianomontone@gmail.com>"
  :license  "MIT"
  :version "0.0.1"
  :serial t
  :components ((:module "test"
                        :components
                        ((:file "package")
                         (:file "test")
                         (:file "parser")
                         (:file "assist"))))
  :depends-on (:clgrs :fiveam)
  :perform (asdf:test-op (op c)
                         (uiop:symbol-call :clgrs/test :run-tests)))
