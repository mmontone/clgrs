(asdf:defsystem #:clgrs.asdf
  :description "Common Lisp Grammar Services for ASDF"
  :author "Mariano Montone <marianomontone@gmail.com>"
  :license  "MIT"
  :version "0.0.1"
  :serial t
  :components ((:module "src"
                        :components
                        ((:file "asdf"))))
  :depends-on (:clgrs :clavier))
