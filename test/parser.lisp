(in-package :clgrs/test)

(in-suite clgrs-tests)

(defun parse-rule (rule form)
  (clgrs/parser::parse-rule* (make-grammar-node rule) form))

(defmacro parse-fail (rule form)
  `(signals clgrs/parser::grammar-parse-error
     (parse-rule ,rule ,form)))

(defmacro parse-success (rule form &optional (result form))
  `(is (equalp (parse-rule ,rule ,form) ,result)))

(test parse-integer-test
  (parse-success 'integer 22)
  (parse-fail 'integer "22"))

(test parse-symbol-test
  (parse-success 'symbol 'a-symbol)
  (parse-fail 'symbol 22))

(test parse-string-test
  (parse-success 'string "hello")
  (parse-fail 'string 'symbol))

(test parse-list-test
  (parse-success '(list) '())
  (parse-success '(list symbol) '(foo))
  (parse-fail '(list) 'symbol)
  (parse-fail '(list string integer) '("lala"))
  ;; Junk allowed?
  ;;(parse-fail '(list string) '("lala" 22))
  )

(test parse-optional-test
  (parse-success '(list (? string)) '("foo"))
  (parse-success '(list (? string)) '() '(nil)))

(test parse-seq-test
  (parse-success '(list (seq string integer)) '("foo" 22))
  (parse-fail '(list (seq string integer)) '(22 "foo"))
  (parse-fail '(list (seq string integer)) '("foo"))
  ;; Junk allowed?
  ;;(parse-fail '(list (seq string integer)) '("foo" 22 "bar"))
  )

(test parse-or-test
  (parse-success '(or string symbol) "foo")
  (parse-success '(or string symbol) 'foo)
  (parse-fail '(or string symbol) 22))

(test parse-many-test
  (parse-success '(list (* symbol)) '(foo bar))
  (parse-success '(list (* (or symbol integer)) string)
                 '(foo 2 bar "foo"))
  (parse-success '(list (* symbol) integer) '(22))
  (parse-success '(list (* integer)) '()))

(test parse-many-plus-test
  (parse-success '(list (+ symbol)) '(foo))
  (parse-success '(list (+ (or symbol integer))) '(foo 22 bar))
  (parse-success '(list (+ (or symbol integer)) string)
                 '(foo 2 "foo"))
  (parse-fail '(list (+ symbol)) '()))

(test parse-negative-test
  (parse-success '(not symbol) "foo"))

(test parse-followed-by-test
  ;; Note this pattern!!
  (parse-success '(list (* symbol) (& (list))) '(foo))
  (parse-fail '(list (* symbol) (& (list))) '(foo "string"))
  (parse-success '(list (& symbol) symbol) '(foo)))

(test parse-not-followed-by-test
  ;; Careful. ! is exported in fiveam.
  (parse-success '(list (clgrs::! symbol)) '()))
