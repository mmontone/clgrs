(in-package :clgrs/test)

(def-suite clgrs-tests)

(defun run-tests (&key debug)
  (if debug
      (debug! 'clgrs-tests)
      (run 'clgrs-tests)))
