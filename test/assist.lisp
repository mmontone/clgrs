(in-package :clgrs/test)

(in-suite clgrs-tests)

(defun assist-rule (rule form)
  (clgrs/assist::assist-rule (clgrs:make-grammar-node rule) form))

(defrule topts
    (or (and :symbol symbol)
        (and :string string)
        (and :integer integer)
        ;;(and :boolean boolean)
        ;;(and :pathname pathname)
        ))

(test assist-test
  (let ((alternatives
         (assist-rule '(list symbol)
                      `(,(clgrs/assist::make-assist-marker)))))
    (is (some
         (lambda (item)
           (and (eql (clgrs/assist::assist-item-action item) :enter)
                (eql (clgrs/assist::assist-item-type item) :symbol)))
         alternatives)))

  (let ((alternatives
         (assist-rule '(list symbol)
                      `(,(clgrs/assist::make-assist-marker)))))
    (is (some
         (lambda (item)
           (and (eql (clgrs/assist::assist-item-action item) :enter)
                (eql (clgrs/assist::assist-item-type item) :symbol)))
         alternatives)))

  (let ((alternatives
         (assist-rule '(list symbol string)
                      `(foo ,(clgrs/assist::make-assist-marker)))))
    (is (some
         (lambda (item)
           (and (eql (clgrs/assist::assist-item-action item) :enter)
                (eql (clgrs/assist::assist-item-type item) :string)))
         alternatives)))

  ;; or-node

  (let ((alternatives
         (assist-rule '(or integer string) `(,(clgrs/assist::make-assist-marker)))))
    (is
     (some (lambda (item)
             (and (eql (clgrs/assist::assist-item-action item) :enter)
                  (eql (clgrs/assist::assist-item-type item) :rule)
                  (typep (clgrs/assist::assist-item-rule item) 'clgrs::integer-node)))
           alternatives))

    (is
     (some (lambda (item)
             (and (eql (clgrs/assist::assist-item-action item) :enter)
                  (eql (clgrs/assist::assist-item-type item) :rule)
                  (typep (clgrs/assist::assist-item-rule item) 'clgrs::integer-node)))
           alternatives)))

  (let ((alternatives
         (assist-rule '(list (* topts)) `(:symbol ,(clgrs/assist::make-assist-marker)))))
    (is (some (lambda (item)
                (and (eql (clgrs/assist::assist-item-action item) :enter)
                     (eql (clgrs/assist::assist-item-type item) :symbol)))
              alternatives)))

  (let ((alternatives
         (assist-rule '(list (* topts)) `(:string ,(clgrs/assist::make-assist-marker)))))
    (is (some (lambda (item)
                (and (eql (clgrs/assist::assist-item-action item) :enter)
                     (eql (clgrs/assist::assist-item-type item) :string)))
              alternatives)))
  
  (is (assist-rule '(list (* topts)) `(,(clgrs/assist::make-assist-marker))))
  (is (not (assist-rule '(list (* topts)) `(:foo ,(clgrs/assist::make-assist-marker))))))

(assist-rule '(list (seq :option symbol)) `(:option ,(clgrs/assist::make-assist-marker)))

(assist-rule '(list (or (seq :option symbol) (seq :foo string)))
             `(:foo ,(clgrs/assist::make-assist-marker)))

;; Or :class :instance
;; (clgrs/assist::assist-rule
;;  (clgrs::find-grammar-rule 'clgrs/clos::class-slot)
;;  `(slot-name :allocation ,(clgrs/assist::make-assist-marker)))
