(asdf:defsystem #:clgrs.clos
  :description "Common Lisp Grammar Services for CLOS"
  :author "Mariano Montone <marianomontone@gmail.com>"
  :license  "MIT"
  :version "0.0.1"
  :serial t
  :components ((:module "src"
                        :components
                        ((:file "clos"))))
  :depends-on (:clgrs))
