(in-package :clgrs)

(defgrammar (defgrammar
                :documentation "Define a CLGRS grammar")
    (list ($ defgrammar)
          grammar-name-and-options
          grammar))

(defrule grammar-name
    (@ symbol :name 'grammar-name))

(defrule grammar-name-and-options
    (or grammar-name
        (list grammar-name (@ (* grammar-option) :name 'options))))

(defrule grammar-option
    (or (and :documentation string)
        (and :title string)))

(defrule grammar any)

(defgrammar (defrule
                :documentation "Define a CLGRS rule")
    (list ($ defrule)
          rule-name-and-options
          rule))

(defrule rule any)
(defrule rule-name
    (@ symbol :name 'rule-name))

(defrule rule-name-and-options
    (or rule-name
        (list rule-name (@ (* rule-option) :name 'options))))

(defrule rule-option
    (or (and :name symbol)
        (and :documentation string)))
