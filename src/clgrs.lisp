(in-package #:clgrs)

(defvar *grammars* (make-hash-table))

(defclass grammar ()
  ((name :initarg :name
         :accessor grammar-name
         :type symbol)
   (grammar :initarg :grammar)
   (documentation :initarg :documentation
                  :accessor grammar-documentation
                  :initform nil)
   (title :initarg :title
          :accessor grammar-title
          :type (or null string)
          :initform nil)))
         
(defmacro defgrammar (name-and-options grammar)
  (multiple-value-bind (name options)
      (if (listp name-and-options)
          (values (car name-and-options) (cdr name-and-options))
          (values name-and-options nil))
    `(setf (gethash ',name *grammars*)
           (make-instance 'grammar
                          :name ',name
                          :grammar (lambda () ,(compile-grammar-node grammar))
                          ,@options))))

(defun find-grammar (name &optional (error-p t))
  (or (gethash name *grammars*)
      (and error-p
           (error "Grammar not defined: ~A" name))))

(defmethod grammar ((grammar grammar))
  (funcall (slot-value grammar 'grammar)))

(defmacro defrule (name-and-options rule)
  (multiple-value-bind (name options)
      (if (listp name-and-options)
          (values (car name-and-options) (cdr name-and-options))
          (values name-and-options nil))
    `(setf (get ',name :grammar-rule)
           (lambda () ,(compile-grammar-node `(@ ,rule ,@options))))))

(defmacro set-symbol-rule (symbol rule)
  `(setf (get ',symbol :grammar-rule)
         (lambda () ,rule)))

(defun find-grammar-rule (name)
  (or (let ((r (get name :grammar-rule)))
        (and r (funcall r)))
      (error "Grammar rule not defined: ~A" name)))

;; Nodes

(defclass with-children ()
  ((children :initarg :children
             :accessor children
             :type list
             :initform nil)))

(defmethod initialize-instance :after ((node with-children) &rest initargs)
  (dolist (child (children node))
    (setf (node-parent child) node)))

(defclass with-child ()
  ((child :initarg :child
          :accessor child
          :type node
          :initform nil)))

(defmethod initialize-instance :after ((node with-child) &rest initargs)
  (setf (node-parent (child node)) node))

(defclass node ()
  ((name :initarg :name
         :type (or symbol null)
         :accessor node-name
         :initform nil)
   (title :initarg :title
          :accessor node-title
          :type (or null string)
          :initform nil)
   (parent :initarg :parent
           :accessor node-parent
           :type (or null node)
           :initform nil
           :documentation "The node parent")
   (documentation :initarg :documentation
                  :accessor node-documentation
                  :type (or null string)
                  :initform nil)
   (validator :initarg :validator
              :accessor node-validator
              :initform nil)
   (completer :initarg :completer
              :accessor node-completer
              :initform nil)
   (action :initarg :action
           :accessor node-action
           :initform (lambda (rule form) nil)
           :documentation "Function that is executed after a succesful parsing.")
   (form :initarg :form
         :accessor node-form
         :initform nil
         :documentation "This slot (sometimes) contains the form(s) being parsed. NOT necessarily the form(s) successfully parsed.")
   (font-face :initarg :font-face
              :accessor node-font-face
              :type (or null (member :keyword :type :function :identifier))
              :initform nil
              :documentation "Type of font-face. Used for deriving syntax highlighting rules")
   (format :initarg :format
           :accessor node-format
           :initform nil
           :documentation "Node formatting (layout) hint")))

(defun set-node-properties (node properties)
  (alexandria:doplist (prop val properties)
    (set-node-property node prop val))
  node)

(defgeneric set-node-property (node prop val))

(defmacro def-node-property (prop accessor)
  `(defmethod set-node-property ((node node) (prop (eql ,prop))
                                 val)
     (setf (,accessor node) val)))

(def-node-property :title node-title)
(def-node-property :documentation node-documentation)
(def-node-property :validator node-validator)
(def-node-property :completer node-completer)
(def-node-property :name node-name)
(def-node-property :action node-action)
(def-node-property :font-face node-font-face)
(def-node-property :format node-format)

(defmacro def-grammar-form (symbol compile interpret)
  `(progn
     (setf (get ',symbol :compile-grammar-form)
           (lambda (form)
             ,compile))
     (setf (get ',symbol :interpret-grammar-form)
           (lambda (form)
             ,interpret))))

(def-grammar-form seq
    `(make-instance 'seq-node
                    :children
                    (list ,@(mapcar 'compile-grammar-node (cdr form))))
  (make-instance 'seq-node
                 :children
                 (mapcar 'make-grammar-node (cdr form))))

(def-grammar-form and
    `(make-instance 'seq-node
                    :children
                    (list ,@(mapcar 'compile-grammar-node (cdr form))))
  (make-instance 'seq-node
                 :children
                 (mapcar 'make-grammar-node (cdr form))))

(def-grammar-form list
    `(make-instance 'list-node
                    :children
                    (list ,@(mapcar 'compile-grammar-node (cdr form))))
  (make-instance 'list-node
                 :children
                 (mapcar 'make-grammar-node (cdr form))))

(def-grammar-form sym
    `(make-instance 'symbol-node
                            :symbol ',(cadr form))
  (make-instance 'symbol-node
                 :symbol (cadr form)))

(def-grammar-form $
    `(make-instance 'symbol-node
                            :symbol ',(cadr form))
  (make-instance 'symbol-node
                 :symbol (cadr form)))

(def-grammar-form keyword
    `(make-instance 'keyword-node
                    :keyword ',(cadr form))
  (make-instance 'keyword-node
                 :keyword (cadr form)))

(def-grammar-form ?
    `(make-instance 'optional-node
                          :child ,(compile-grammar-node (cadr form)))
  (make-instance 'optional-node
                 :child (make-grammar-node (cadr form))))

(def-grammar-form *
    `(make-instance 'many-node
                          :child
                          ,(compile-grammar-node (cadr form)))
    (make-instance 'many-node
                         :child
                         (make-grammar-node (cadr form))))

(def-grammar-form +
    `(make-instance 'many-plus-node
                          :child
                          ,(compile-grammar-node (cadr form)))
    (make-instance 'many-plus-node
                         :child
                         (make-grammar-node (cadr form))))

(def-grammar-form or
    `(make-instance 'or-node
                           :children
                           (list ,@(mapcar 'compile-grammar-node (cdr form))))
    (make-instance 'or-node
                           :children
                           (mapcar 'make-grammar-node (cdr form))))

(def-grammar-form @
    (let ((node (compile-grammar-node (cadr form))))
      (concatenate 'list node (cddr form)))
    (set-node-properties (make-grammar-node (cadr form))
                         (cddr form)))

(def-grammar-form str
    `(make-instance 'string-node
                    :string ',(cadr form))
  (make-instance 'string-node
                 :string (cadr form)))

(def-grammar-form int
    `(make-instance 'int-node
                    :integer ',(cadr form))
  (make-instance 'int-node
                 :integer (cadr form)))

(def-grammar-form bool
    `(make-instance 'boolean-node
                    :boolean ',(cadr form))
  (make-instance 'boolean-node
                 :boolean (cadr form)))

(def-grammar-form member
    `(make-instance 'member-node
                    :members ',(cdr form))
  (make-instance 'member-node
                 :members (cdr form)))

(def-grammar-form not
    `(make-instance 'negative-node
                    :child
                    ,(compile-grammar-node (cadr form)))
  (make-instance 'negative-node
                 :child
                 (make-grammar-node (cadr form))))

(def-grammar-form &
    `(make-instance 'followed-by-node
                    :child
                    ,(compile-grammar-node (cadr form)))
  (make-instance 'followed-by-node
                 :child
                 (make-grammar-node (cadr form))))

(def-grammar-form !
    `(make-instance 'not-followed-by-node
                    :child
                    ,(compile-grammar-node (cadr form)))
  (make-instance 'not-followed-by-node
                 :child
                 (make-grammar-node (cadr form))))

(defun make-grammar-node (form)
  (cond
    ((listp form)
     (let ((rule (get (car form) :interpret-grammar-form)))
       (if rule
           (funcall rule form)
           (error "Invalid grammar node: ~S" form))))
    ((keywordp form)
     (make-instance 'keyword-node :keyword form))
    ((symbolp form)
     (find-grammar-rule form))
    (t (error "Invalid grammar node: ~S" form))))

(defun compile-grammar-node (form)
  (cond
    ((listp form)
     (let ((rule (get (car form) :compile-grammar-form)))
       (if rule
           (funcall rule form)
           (error "Invalid grammar node: ~S" form))))
    ((eql form 'any)
     `(make-instance 'any-node))
    ((keywordp form)
     `(make-instance 'keyword-node :keyword ,form))
    ((symbolp form)
     `(make-instance 'rule-node :rule ',form))
    (t (error "Invalid grammar node: ~S" form))))

(defclass or-node (node with-children)
  ()
  ;;(:default-initargs :name 'or)
  )

(defclass optional-node (node with-child)
  ()
  ;;(:default-initargs :name '?)
  )

(defclass many-node (node with-child)
  ()
  ;;(:default-initargs :name '*)
  )

(defclass many-plus-node (node with-child)
  ()
  ;;(:default-initargs :name '+)
  )

(defclass list-node (node with-children)
  ()
  ;;(:default-initargs :name 'list)
  (:documentation "A list of nodes"))

(defclass seq-node (node with-children)
  ()
  ;;(:default-initargs :name 'seq)
  (:documentation "A spliced sequence of nodes"))

(defclass negative-node (node with-child)
  ()
  (:documentation "Succeeds when child node fails, fails otherwise."))

(defclass rule-node (node)
  ((rule :initarg :rule
         :type symbol)))

(defmethod node-rule ((node rule-node))
  (let ((rule (find-grammar-rule (slot-value node 'rule))))
    (let ((node-name (or (node-name rule) (node-name node)))
          (node-doc (or (node-documentation rule) (node-documentation node)))
          (node-completer (or (node-completer rule) (node-completer node)))
          (node-format (append (node-format rule) (node-format node))))
    
      (setf (node-name rule) node-name
            (node-name node) node-name
            (node-documentation rule) node-doc
            (node-documentation node) node-doc
            (node-completer rule) node-completer
            (node-completer node) node-completer
            (node-format rule) node-format
            (node-format node) node-format)
    rule)))    

(defclass symbol-node (node)
  ((symbol :initarg :symbol
           :accessor node-symbol
           :initform nil)))

(defclass keyword-node (node)
  ((keyword :initarg :keyword
           :accessor node-keyword
           :initform nil)))

(defclass string-node (node)
  ((string :initarg :string
           :accessor node-string
           :initform nil)))

(defclass integer-node (node)
  ((integer :initarg :integer
            :accessor node-integer
            :initform nil)))

(defclass boolean-node (node)
  ((boolean :initarg :boolean
            :accessor node-boolean
            :initform nil)))

(defclass pathname-node (node)
  ((pathname :initarg :pathname
             :accessor node-pathname
             :initform nil)))

(defclass predicate-node (node)
  ((predicate :initarg :predicate
              :accessor node-predicate)))

(defclass member-node (node)
  ((members :initarg :members
            :accessor node-members)))

(defclass any-node (node)
  ())

(defclass followed-by-node (node with-child)
  ()
  (:documentation "Followed by. Does not consume."))

(defclass not-followed-by-node (node with-child)
  ()
  (:documentation "Not followed by. Does not consume."))

(set-symbol-rule sym (make-instance 'symbol-node))
(set-symbol-rule symbol (make-instance 'symbol-node))
(set-symbol-rule str (make-instance 'string-node))
(set-symbol-rule string (make-instance 'string-node))
(set-symbol-rule int (make-instance 'integer-node))
(set-symbol-rule integer (make-instance 'integer-node))
(set-symbol-rule bool (make-instance 'boolean-node))
(set-symbol-rule boolean (make-instance 'boolean-node))
(set-symbol-rule pathname (make-instance 'pathname-node))
(set-symbol-rule any (make-instance 'any-node))
(set-symbol-rule keyword (make-instance 'keyword-node))

(defstruct grammar-marker
  form)

(define-condition grammar-marker-found (simple-condition)
  ((marker :initarg :marker
           :accessor marker)
   (rule :initarg :rule
         :accessor rule)
   (depth :initarg :depth
          :accessor depth)))

;; Utility functions

(defun slot-doc (slot-name class &optional (display-type t))
  (let ((cls (if (symbolp class)
                 (find-class class)
                 class)))
    (c2mop:finalize-inheritance cls)
    (let ((slot (find slot-name (c2mop:class-slots cls)
                      :key 'c2mop:slot-definition-name)))
      (if (not display-type)
          (documentation slot t)
          (format nil "~A~%~%Type: ~A"
                  (documentation slot t)
                  (c2mop:slot-definition-type slot))))))
