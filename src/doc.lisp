(defpackage :clgrs/doc
  (:use :cl :clgrs))

(in-package :clgrs/doc)

(defstruct (doc-marker
             (:include clgrs::grammar-marker)))

(defun look-doc-in-context ()
  (loop
     for node in clgrs/parser::*rule-chain*
     when (clgrs::node-documentation node)
     do (return-from look-doc-in-context (clgrs::node-documentation node)))
  nil)

(defun doc (grammar form &optional look-in-context)
  (handler-bind
      ((clgrs::grammar-marker-found
        (lambda (gmf)
          (return-from doc
            (values
             (if look-in-context
                 (look-doc-in-context)
                 (clgrs::node-documentation (first clgrs/parser::*rule-chain*)))
             clgrs/parser::*rule-chain*)))))
    (clgrs/parser::parse-grammar grammar form))
  nil)

;; (doc (clgrs::find-grammar 'clgrs/asdf::defsystem)
;;      '(asdf:defsystem #.(make-doc-marker))
;;      t)
