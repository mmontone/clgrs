;; defpackage syntax
;; http://clhs.lisp.se/Body/m_defpkg.htm

(in-package :clgrs)

(defun defpackage/list-packages ()
  (let ((*print-case* :downcase))
    (mapcar (lambda (p)
              (list :message (string-downcase (package-name p))
                    :value (prin1-to-string (alexandria:make-keyword (package-name p)))
                    :documentation (documentation p t)))
          (list-all-packages))))

(defun package-documentation (package)
  (documentation package t))

(defgrammar (defpackage :documentation "defpackage creates a package as specified and returns the package.")
    (list ($ defpackage) (@ symbol :name 'package-name
                                   :format '(:after (:newline :indent)))
          (@ package-options :name 'package-options)))

(defrule package-options
    (* (@ package-option :format '(:after :newline))))

(defrule (package-name
          :documentation "Package name")
    (@ symbol :name 'package-name
       :completer 'defpackage/list-packages))

(defrule package-option
    (or
     (list :nicknames (@ (* string-designator) :name 'nicknames))
     (list :documentation (@ string :name 'documentation))
     (list :use (@ (* package-name) :name 'use-packages))
     (@ (list :shadow (@ (* symbol) :name 'shadow))
        :name 'shadow-symbols
        :documentation "Symbols with names given are present in the package. Specifically, package is searched for symbols with the names supplied by symbol-names. For each such name, if a corresponding symbol is not present in package (directly, not by inheritance), then a corresponding symbol is created with that name, and inserted into package as an internal symbol. The corresponding symbol, whether pre-existing or newly created, is then added, if not already present, to the shadowing symbols list of package. http://clhs.lisp.se/Body/f_shadow.htm#shadow")
     (@ (list :shadowing-import-from package-name (* string-designator))
        :name 'shadowing-import-from
        :documentation "The symbols named by the argument symbol-names are found (involving a lookup as if by find-symbol) in the specified package-name. The resulting symbols are imported into the package being defined, and placed on the shadowing symbols list as if by shadowing-import. In no case are symbols created in any package other than the one being defined.")
     (@ (list :export (* string-designator)) :name 'export)
     (@ (list :intern (* string-designator)) :name 'intern)
     (@ (list :size (@ integer :name 'size))
        :documentation "The argument to the :size option declares the approximate number of symbols expected in the package. This is an efficiency hint only and might be ignored by an implementation.")))

(defrule (string-designator
          :documentation "A designator for a string; that is, an object that denotes a string and that is one of: a character (denoting a singleton string that has the character as its only element), a symbol (denoting the string that is its name), or a string (denoting itself). The intent is that this term be consistent with the behavior of string; implementations that extend string must extend the meaning of this term in a compatible way. http://clhs.lisp.se/Body/26_glo_s.htm#string_designator")
    ;; We disable strings for now, to make completion better
    ;;(or string symbol)
    symbol
    )
