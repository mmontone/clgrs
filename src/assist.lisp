(defpackage :clgrs/assist
  (:use :cl :clgrs))

(in-package :clgrs/assist)

(defvar *debug-on-error* nil)

(defstruct assist-item
  name
  action
  type
  rule
  message
  documentation
  text
  snippet)

(defstruct (assist-marker
             (:include clgrs::grammar-marker)))

(defun assist-form (form)
  "Match FORM with an existent grammar. If grammar is found,
return assist alternatives."
  (let ((grammar (clgrs::find-grammar (first form) nil)))
    (when grammar
      (assist grammar form))))

(defun emacs-assist-form (form)
  (let ((*print-case* :downcase)
        (res (if *debug-on-error*
                 (assist-form form)
                 (handler-case
                     (assist-form form)
                   (error ()
                     :error)))))    
    (->elisp res)))

(defun emacs-list-grammars ()
  (let ((grammars (alexandria:hash-table-values clgrs::*grammars*)))
    (->elisp grammars)))

(defun assist (grammar form)
  (handler-bind
      ((clgrs::grammar-marker-found
        (lambda (gmf)
          (return-from assist
            (values
             (node-assist (clgrs::rule gmf)
                          (clgrs::marker gmf))
             clgrs/parser::*rule-chain*)))))
    (clgrs/parser::parse-grammar grammar form))
  nil)

(defun assist-rule (rule form)
  (handler-bind
      ((clgrs::grammar-marker-found
        (lambda (gmf)
          (return-from assist-rule
            (values
             (node-assist (clgrs::rule gmf)
                          (clgrs::marker gmf))
             clgrs/parser::*rule-chain*)))))
    (clgrs/parser::parse-rule* rule form))
  nil)

(defgeneric node-assist (node assist-marker))

(defmethod node-assist (node assist-marker)
  (list
   (make-assist-item
    :action :enter
    :rule node
    :message (assist-template* node)
    :documentation (clgrs::node-documentation node)
    :text (assist-template* node)
    :snippet (assist-snippet* node))))

(defmethod node-assist ((node clgrs::many-node) assist-marker)
  (list :add (clgrs::child node)))

(defmethod node-assist ((node clgrs::symbol-node) assist-marker)
  (cond
    ((assist-marker-form assist-marker)
     ;; Cursor over symbol. Offer to SET-VALUE (replace).
     (list (make-assist-item
            :action :set-value
            :type :symbol
            :rule node
            :message (if (clgrs::node-name node)
                         (format nil "~A (symbol)" (clgrs::node-name node))
                         "symbol")
            :documentation (clgrs::node-documentation node)
            :text (clgrs::node-name node)
            :snippet (assist-snippet* node)
            )))
    ((clgrs::node-symbol node)
     ;; Append the node symbol
     (list (make-assist-item
            :action :append
            :type :symbol
            :rule node
            :message (princ-to-string (clgrs::node-symbol node))
            :documentation (clgrs::node-documentation node)
            :text (clgrs::node-symbol node)
            :snippet (assist-snippet* node))))
    (t (list
        (make-assist-item
         :action :enter
         :type :symbol
         :rule node
         :message (if (clgrs::node-name node)
                      (format nil "~A (symbol)" (clgrs::node-name node))
                      "symbol")
         :documentation (clgrs::node-documentation node)
         :text (clgrs::node-name node)
         :snippet (assist-snippet* node))))))

(defmethod node-assist ((node clgrs::string-node) assist-marker)
  (cond
    ((assist-marker-form assist-marker)
     ;; Cursor over symbol. Offer to SET-VALUE (replace).
     (list (make-assist-item
            :action :set-value
            :type :string
            :rule node
            :message (if (clgrs::node-name node)
                         (format nil "~A (string)" (clgrs::node-name node))
                         "string")
            :documentation (clgrs::node-documentation node)
            :text (clgrs::node-name node)
            :snippet (assist-snippet* node))))
    ((clgrs::node-string node)
     ;; Append the node string
     (list (make-assist-item
            :action :append
            :type :string
            :rule node
            :message (princ-to-string (clgrs::node-symbol node))
            :documentation (clgrs::node-documentation node)
            :text (clgrs::node-symbol node)
            :snippet (assist-snippet* node))))
    (t (list
        (make-assist-item
         :action :enter
         :type :string
         :rule node
         :message (if (clgrs::node-name node)
                      (format nil "~A (string)" (clgrs::node-name node))
                      "string")
         :documentation (clgrs::node-documentation node)
         :text (clgrs::node-name node)
         :snippet (assist-snippet* node))))))

(defmethod node-assist ((node clgrs::integer-node) assist-marker)
  (cond
    ((assist-marker-form assist-marker)
     ;; Cursor over symbol. Offer to SET-VALUE (replace).
     (list (make-assist-item
            :action :set-value
            :type :integer
            :rule node
            :message (if (clgrs::node-name node)
                         (format nil "~A (integer)" (clgrs::node-name node))
                         "integer")
            :documentation (clgrs::node-documentation node)
            :text (clgrs::node-name node)
            :snippet (assist-snippet* node))))
    ((clgrs::node-integer node)
     ;; Append the node integer
     (list (make-assist-item
            :action :append
            :type :integer
            :rule node
            :message (princ-to-integer (clgrs::node-symbol node))
            :documentation (clgrs::node-documentation node)
            :text (clgrs::node-symbol node)
            :snippet (assist-snippet* node))))
    (t (list
        (make-assist-item
         :action :enter
         :type :integer
         :rule node
         :message (if (clgrs::node-name node)
                      (format nil "~A (integer)" (clgrs::node-name node))
                      "integer")
         :documentation (clgrs::node-documentation node)
         :text (clgrs::node-name node)
         :snippet (assist-snippet* node))))))

(defmethod node-assist ((node clgrs::or-node) assist-marker)
  (loop
     for node in (clgrs::children node)
     appending
       (node-assist node assist-marker)))

(defun assist-interactive (grammar form)
  (let ((options (assist grammar form)))
    ()))

(defun assist-template* (node)
  (let ((*print-case* :downcase))
    (with-output-to-string (out)
      (assist-template node out))))

(defgeneric assist-template (node stream))

(defmethod assist-template ((node clgrs::symbol-node) stream)
  (format stream "~A"
          (or (clgrs::node-symbol node)
              (clgrs::node-name node)
              'symbol)))

(defmethod assist-template ((node clgrs::keyword-node) stream)
  (format stream "~S"
          (or (clgrs::node-keyword node)
              (clgrs::node-name node)
              :keyword)))

(defmethod assist-template ((node clgrs::string-node) stream)
  (format stream "~S"
          (or (clgrs::node-string node)
              (clgrs::node-name node)
              'string)))

(defmethod assist-template ((node clgrs::integer-node) stream)
  (format stream "~A"
          (or (clgrs::node-integer node)
              (clgrs::node-name node)
              'integer)))

(defmethod assist-template ((node clgrs::boolean-node) stream)
  (format stream "~A"
          (or (clgrs::node-boolean node)
              (clgrs::node-name node)
              'boolean)))

(defmethod assist-template ((node clgrs::list-node) stream)

  (write-string "(" stream)

  (let ((children (clgrs::children node)))
    (when (first children)
      (assist-template (first children) stream))
    (loop
       for child in (cdr children)
       do
         (write-char #\space stream)
         (assist-template child stream)))

  (write-string ")" stream))

(defmethod assist-template ((node clgrs::or-node) stream)
  (princ (or (clgrs::node-name node)
             "...")
         stream))

(defmethod assist-template ((node clgrs::rule-node) stream)
  (if (clgrs::node-name node)
      (princ (clgrs::node-name node) stream)
      (assist-template (clgrs::node-rule node) stream)))

(defmethod assist-template ((node clgrs::many-node) stream)
  (if (clgrs::node-name node)
      (princ (clgrs::node-name node) stream)
      (progn
        (assist-template (clgrs::child node) stream)
        (write-char #\* stream))))

(defmethod assist-template ((node clgrs::seq-node) stream)
  (if (clgrs::node-name node)
      (princ (clgrs::node-name node) stream)
      (progn
        (when (first (clgrs::children node))
          (assist-template (first (clgrs::children node)) stream))
        (loop for child in (cdr (clgrs::children node))
           do
             (write-char #\space stream)
             (assist-template child stream)))))

(defmethod assist-template ((node clgrs::optional-node) stream)
  (write-char #\[ stream)
  (if (clgrs::node-name node)
      (princ (clgrs::node-name node) stream)
      (assist-template (clgrs::child node) stream))
  (write-char #\] stream))

(defmethod assist-template ((node clgrs::any-node) stream)
  (princ (or (clgrs::node-name node)
             "...")
         stream))


;; Assist snippets (Emacs yasnippet)

(defvar *snippet-n* 0)

(defun next-snippet-n ()
  (incf *snippet-n*))

#+nil(defun assist-snippet* (node)
  (let ((*snippet-n* 0)
        (*print-case* :downcase))
    (with-output-to-string (out)
      (assist-snippet node out))))

(defun assist-snippet* (node)
  (simple-snippet* node))

(defgeneric assist-snippet (node stream))

(defmethod assist-snippet ((node clgrs::symbol-node) stream)
  (if (clgrs::node-symbol node)
      (prin1 (clgrs::node-symbol node) stream)
      (format stream "${~A:~A}"
              (next-snippet-n)
              (or
               (clgrs::node-name node)
               'symbol))))

(defmethod assist-snippet ((node clgrs::keyword-node) stream)
  (if (clgrs::node-keyword node)
      (prin1 (clgrs::node-keyword node) stream)
      (format stream ":${~A:~A}"
              (next-snippet-n)
              (or (clgrs::node-name node)
                  'keyword))))

(defmethod assist-snippet ((node clgrs::string-node) stream)
  (if (clgrs::node-string node)
      (prin1 (clgrs::node-string node) stream)
      (format stream "\"${~A:~A}\""
              (next-snippet-n)
              (or (clgrs::node-name node)
                  "string"))))

(defmethod assist-snippet ((node clgrs::integer-node) stream)
  (format stream "${~A:~S}"
          (next-snippet-n)
          (or (clgrs::node-integer node)
              (clgrs::node-name node)
              'integer)))

(defmethod assist-snippet ((node clgrs::boolean-node) stream)
  (format stream "~A"
          (or (clgrs::node-boolean node)
              (clgrs::node-name node)
              'boolean)))

(defmethod assist-snippet ((node clgrs::list-node) stream)

  (write-string "(" stream)

  (let ((children (clgrs::children node)))
    (when (first children)
      (assist-snippet (first children) stream))
    (loop
       for child in (cdr children)
       do
         (write-char #\space stream)
         (assist-snippet child stream)))

  (write-string ")" stream))

(defmethod assist-snippet ((node clgrs::or-node) stream)
  (format stream "${~A:~A$(clgrs/yas-assist)}"
          (next-snippet-n)
          (or (clgrs::node-name node)
              "_")))

(defmethod assist-snippet ((node clgrs::rule-node) stream)
  (if (and (clgrs::node-name node) nil)
      (format stream "${~A:~A}"
              (next-snippet-n)
              (clgrs::node-name node))
      (assist-snippet (clgrs::node-rule node) stream)))

(defmethod assist-snippet ((node clgrs::many-node) stream)
  (format stream "${~A:~A$(clgrs/yas-assist)}"
          (next-snippet-n)
          (or (clgrs::node-name node)
              (with-output-to-string (s)
                (assist-template (clgrs::child node) s)
                (write-char #\* s)))))

(defmethod assist-snippet ((node clgrs::seq-node) stream)
  (if (and (clgrs::node-name node) nil)
      (princ (clgrs::node-name node) stream)
      (progn
        (when (first (clgrs::children node))
          (assist-snippet (first (clgrs::children node)) stream))
        (loop for child in (cdr (clgrs::children node))
           do
             (write-char #\space stream)
             (assist-snippet child stream)))))

(defmethod assist-snippet ((node clgrs::any-node) stream)
  (format stream "${~A:~A$(clgrs/yas-assist)}"
          (next-snippet-n)
          (or
           (clgrs::node-name node)
           "_")))

(defmethod assist-snippet ((node clgrs::optional-node) stream)
  (format stream "${~A:[~A]$(clgrs/yas-optional)}"
          (next-snippet-n)
          (with-output-to-string (s)
            (assist-template (clgrs::child node) s))))

;; Serialize to Emacs

(defgeneric ->elisp (thing))

(defmethod ->elisp (thing)
  thing)

(defmethod ->elisp ((node clgrs::node))
  (type-of node))

(defmethod ->elisp ((item assist-item))
  (list :name (assist-item-name item)
        :action (assist-item-action item)
        :type (->elisp (assist-item-type item))
        :rule (->elisp (assist-item-rule item))
        :message (assist-item-message item)
        :documentation (assist-item-documentation item)
        :text (assist-item-text item)
        :snippet (assist-item-snippet item)))

(defmethod ->elisp ((list list))
  (mapcar '->elisp list))

(defmethod ->elisp ((grammar clgrs::grammar))
  (list :name (string-downcase (princ-to-string (clgrs::grammar-name grammar)))
        :message (or (clgrs::grammar-title grammar)
                     (string-downcase (princ-to-string (clgrs::grammar-name grammar))))
        :documentation (clgrs::grammar-documentation grammar)
        :snippet (grammar-snippet grammar)))

(defun grammar-snippet (grammar)
  (assist-snippet* (clgrs::grammar grammar)))
