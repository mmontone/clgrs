(defpackage :clgrs/refactor
  (:use :cl :clgrs))

(in-package :clgrs/refactor)

(defun refactor-alternatives (form)
  (handler-bind
      ((clgrs/parser::grammar-parse-error
        (lambda (gpe)
          (if (typep (clgrs/parser::form gpe) 'refactor-mark)
              (return-from refactor-alternatives
                (values
                 (matching-transforms
                  (first clgrs/parser::*rule-chain*)
                  (clgrs/parser::form gpe))
                 clgrs/parser::*rule-chain*))
              (error gpe)))))
    (clgrs/parser::parse-rule
     (make-instance 'rec-node)
     (lambda () form)
     (lambda (r res)
       (clgrs/parser::parse-success r res))))
  ;; If we get here, it is because there is no refactor-mark
  ;; Find matching transforms for the given form
  (matching-transforms
   (make-instance 'clgrs::node :form form)
   nil :look-up :node))

(defstruct refactor-mark
  form)

(defvar *transform-bindings* nil)

(defclass bind-node (clgrs::node clgrs::with-child)
  ((binding-name :initarg :binding-name
                 :type symbol
                 :accessor binding-name)
   (binding-form :initarg :binding-form
                 :accessor binding-form))
  (:default-initargs :action (lambda (node form)
                               (format t "Binding: ~A" node)
                               (push node
                                     *transform-bindings*))))

(defmethod clgrs/parser::parse-rule ((rule bind-node) next-form success)
  (clgrs/parser::parse-rule
   (clgrs::child rule)
   next-form
   (lambda (r res)
     ;;(clgrs/parser::parse-success rule res)
     (push rule *transform-bindings*)
     (setf (clgrs::node-form rule) res)
     (funcall success r res))))

(clgrs::def-grammar-form =
    `(make-instance 'bind-node
                    :child ,(clgrs::compile-grammar-node (caddr clgrs::form))
                    :binding-name ',(cadr clgrs::form)
                    :binding-form ',(caddr clgrs::form))
  (make-instance 'bind-node
                 :child (clgrs::make-grammar-node (caddr clgrs::form))
                 :binding-name (cadr clgrs::form)
                 :binding-form (caddr clgrs::form)))

(defclass rec-node (clgrs::node)
  ())

(defmethod clgrs/parser::parse-rule ((rule rec-node) next-form success)
  (let ((form (funcall next-form)))
    (setf (clgrs::node-form rule) form)
    (cond
      ((refactor-mark-p form)
       (clgrs/parser::parse-failed rule form "Refactor mark"))
      ((atom form)
       (clgrs/parser::parse-success rule form)
       (funcall success rule form))
      ((listp form)
       (let ((i 0)
             (lst  nil))
         (flet ((nxt-form (&optional (action :next) &rest args)
                  ;;(format t "Next form: ~A at: ~A~%" (nth i form) i)
                  (case action
                    (:next
                     (let ((frm (nth i form)))
                       (incf i)
                       ;;(incf *depth*)
                       ;;(format t "Incremented i: ~A~%" i)
                       frm))
                    (:prev
                     (let ((frm (nth i form)))
                       (decf i)
                       frm))
                    (:peek
                     (nth i form))
                    (:get-index
                     i)
                    (:set-index
                     (setf i (car args)))))
                (succ (rule res)
                  (push res lst)))
           (loop for child in form
              do (clgrs/parser::parse-rule (make-instance 'rec-node)
                                           #'nxt-form #'succ))
           (let ((res (reverse lst)))
             (clgrs/parser::parse-success rule res)
             (funcall success rule res)))))
      (t (clgrs/parser::parse-failed rule form "Parse failed")))))

(clgrs::set-symbol-rule rec (make-instance 'rec-node))

(defvar *transforms* (make-hash-table))

(defstruct transform
  name
  pattern
  function)

(defun matching-transforms (node refactor-mark &key (look-up :top))
  (case look-up
    (:top
     (loop
        for rule in clgrs/parser::*rule-chain*
        appending (node-matching-transforms rule)))
    (:parent
     (let ((parent (second clgrs/parser::*rule-chain*)))
       (append (node-matching-transforms node)
               (when parent
                 (node-matching-transforms parent)))))
    (:node
     (node-matching-transforms node))
    (t (error "LOOK-UP argument is invalid"))))

(defun node-matching-transforms (node)
  (loop
     for transform being the hash-values of *transforms*
     for form := (clgrs::node-form node)
     when (and form (transform-matches-p transform form))
     collect (cons form transform)))

(defun transform-matches-p (transform form)
  ;;(format t "Matching: ~A" form)
  (handler-case
      (clgrs/parser::parse-rule
       (transform-pattern transform)
       (lambda () form)
       (lambda (rule res)
         (return-from transform-matches-p t)))
    (clgrs/parser::grammar-parse-error (e)
      (return-from transform-matches-p nil))))

(defmacro deftransform (name pattern result)
  `(let ((compiled-pattern ,(clgrs::compile-grammar-node pattern)))
     (setf (gethash ',name *transforms*)
           (make-transform
            :name ',name
            :pattern compiled-pattern
            :function
            (lambda (form)
              (let ((*transform-bindings* nil))
                ;; Populate bindings
                (clgrs/parser::parse-rule
                 compiled-pattern
                 (lambda () form)
                 (lambda (rule res)
                   (clgrs/parser::parse-success rule res)))
                (funcall
                 (lambda (bindings)
                   (macrolet
                       (($ (binding-name)
                          `(let ((node (or
                                        (find ',binding-name bindings
                                              :key 'binding-name)
                                        (error "~A is not bound" ',binding-name))))
                             (clgrs::node-form node))))
                     ,result))
                 *transform-bindings*)))))))

(defun apply-transform (form transform)
  (funcall (transform-function transform)
           form))

#+test
(progn

  (deftransform cond->if
    (list (sym cond)
          (list (= test any) (= conseq any))
          (list (sym t) (= altern any)))
  `(if ,($ test) ,($ conseq) ,($ altern)))

  (refactor-alternatives '22)

  (refactor-alternatives
   '(if t
     (print "hello")
     (print "no")))

  (refactor-alternatives
   '(if t
     (print #.(make-refactor-mark))))

  (refactor-alternatives
   '(cond
     (lala lolo)
     (t #.(make-refactor-mark))))

  (let ((alternatives
         (refactor-alternatives
          '(cond
            (lala lolo)
            (t lala)))))
    (destructuring-bind (form . transform) (first alternatives)
      (apply-transform form transform))))
