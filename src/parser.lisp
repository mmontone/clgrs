(defpackage :clgrs/parser
  (:use :cl :clgrs))

(in-package :clgrs/parser)

(defvar *depth* 0)
(defvar *rule-chain* nil)
(defvar *marker-caught* nil)

(define-condition grammar-parse-error (simple-error)
  ((grammar-rule :initarg :rule
                 :accessor grammar-rule)
   (form :initarg :form
         :accessor form)
   (depth :initarg :depth
          :accessor depth)))

(defun grammar-parse-error (grammar-rule form message &rest args)
  (error (make-instance 'grammar-parse-error
                        :rule grammar-rule
                        :form form
                        :depth *depth*
                        :format-control message
                        :format-arguments args)))

(defun parse-failed (grammar-rule form message &rest args)
  (apply #'grammar-parse-error grammar-rule form
         (concatenate 'string message " in: ~A")
         (append args (list (look-name-in-context)))))

(defun parse-success (node result)
  (when (clgrs::node-action node)
    (funcall (clgrs::node-action node) node result)))

(defun parse-grammar (grammar form)
  (parse-rule* (clgrs::grammar grammar) form))

(defun look-name-in-context ()
  (loop
     for rule in *rule-chain*
     when (clgrs::node-name rule)
     do (return-from look-name-in-context (clgrs::node-name rule))))

(defgeneric parse-rule (rule next-form success))

(defmethod parse-rule :around (rule next-form success)
  (push rule *rule-chain*)
  (unwind-protect
       (call-next-method)
    (pop *rule-chain*)))

(defmethod parse-rule ((rule clgrs::symbol-node) next-form success)
  (let ((form (funcall next-form)))
    (setf (clgrs::node-form rule) form)
    (if (null (clgrs::node-symbol rule))
        (cond
          ((and (null form) (clgrs::node-name rule))
           (parse-failed rule form "~A is required" (clgrs::node-name rule)))
          ((and (clgrs::node-name rule) (not (symbolp form)))
           (parse-failed rule form "Invalid ~A. Not a symbol: ~A"
                         (clgrs::node-name rule)
                         form))
          ((or (null form) (not (symbolp form)))
           (parse-failed rule form "Not a symbol: ~s" form)))
        (when (not (eql (clgrs::node-symbol rule) form))
          (if (null form)
              (parse-failed rule form "Expected: ~S" (clgrs::node-symbol rule))
              (parse-failed rule form "Expected: ~s instead of: ~s"
                            (clgrs::node-symbol rule)
                            form))))
    (parse-success rule form)
    (funcall success rule form)))

(defmethod parse-rule ((rule clgrs::keyword-node) next-form success)
  (let ((form (funcall next-form)))
    (setf (clgrs::node-form rule) form)
    (if (null (clgrs::node-keyword rule))
        (cond
          ((and (null form) (clgrs::node-name rule))
           (parse-failed rule form "~A is required" (clgrs::node-name rule)))
          ((and (clgrs::node-name rule) (not (keywordp form)))
           (parse-failed rule form "Invalid ~A. Not a keyword: ~A"
                         (clgrs::node-name rule)
                         form))
          ((or (null form) (not (keywordp form)))
           (parse-failed rule form "Not a keyword: ~s" form)))
        (when (not (eql (clgrs::node-keyword rule) form))
          (if (null form)
              (parse-failed rule form "Expected: ~S" (clgrs::node-keyword rule))
              (parse-failed rule form "Expected: ~s instead of: ~s"
                            (clgrs::node-keyword rule)
                            form))))
    (parse-success rule form)
    (funcall success rule form)))

(defmethod parse-rule ((rule clgrs::list-node) next-form success)
  (let ((form (funcall next-form)))
    (setf (clgrs::node-form rule) form)
    (when (not (listp form))
      (parse-failed rule form "Not a list: ~A" form))
    (let ((i 0)
          (lst nil))
      (flet ((nxt-form (&optional (action :next) &rest args)
               ;;(format t "Next form: ~A at: ~A~%" (nth i form) i)
               (case action
                 (:next
                  (let ((frm (nth i form)))
                    (incf i)
                    (incf *depth*)
                    (when (clgrs::grammar-marker-p frm)
                      (error 'clgrs::grammar-marker-found
                             :marker frm
                             :depth *depth*
                             :rule (first *rule-chain*)))
                    ;;(format t "Incremented i: ~A~%" i)
                    frm))
                 (:prev
                  (let ((frm (nth i form)))
                    (decf i)
                    frm))
                 (:peek
                  (nth i form))
                 (:get-index
                  i)
                 (:set-index
                  (setf i (car args)))))
             (succ (rule res)
               (push res lst)))
        (loop
           for child-rule in (clgrs::children rule)
           do (parse-rule child-rule #'nxt-form #'succ))
        (let ((res (reverse lst)))
          (parse-success rule res)
          (funcall success rule res))))))

(defmethod parse-rule ((rule clgrs::many-node) next-form success)
  (let ((matches nil)
        (child-rule (clgrs::child rule)))
    (handler-case
        (loop
           :while (funcall next-form :peek)
           :do
           (parse-rule child-rule next-form
                       (lambda (rule res)
                         (push res matches)
                         (setf (clgrs::node-form rule) matches)
                         (funcall success rule res))))
      (grammar-parse-error (gpe)
        ;; If there's an error, stop and succeed
        (funcall next-form :prev)
        ))        
    (parse-success rule matches)))

(defmethod parse-rule ((rule clgrs::many-plus-node) next-form success)
  (let ((matches nil)
        (child-rule (clgrs::child rule))
        (form (funcall next-form :peek)))
    (handler-case
        (loop
           :while (funcall next-form :peek)
           :do
           (parse-rule child-rule next-form
                       (lambda (rule res)
                         (push res matches)
                         (setf (clgrs::node-form rule) res)
                         (funcall success rule res))))
      (grammar-parse-error ()
        (funcall next-form :prev)))
    (if (not matches)
        (let ((name (look-name-in-context)))
          (parse-failed rule form "At least one element is expected in ~A" name))
        (parse-success rule matches))))

(defmethod parse-rule ((rule clgrs::string-node) next-form success)
  (let ((form (funcall next-form))
        (node-name (look-name-in-context)))
    (setf (clgrs::node-form rule) form)
    (cond
      ((and (null form) node-name)
       (parse-failed rule form "~A required (string)" node-name))
      ((and node-name (not (stringp form)))
       (parse-failed rule form "Invalid ~A. Not a string: ~A"
                     node-name
                     form))
      ((not (stringp form))
       (parse-failed rule form "Not a string: ~A" form)))
    (parse-success rule form)
    (funcall success rule form)))

(defmethod parse-rule ((rule clgrs::integer-node) next-form success)
  (let ((form (funcall next-form))
        (node-name (look-name-in-context)))
    (setf (clgrs::node-form rule) form)
    (cond
      ((and (null form) node-name)
       (parse-failed rule form "~A required (integer)" node-name))
      ((and node-name (not (numberp form)))
       (parse-failed rule form "Invalid ~A. Not an integer: ~A"
                     node-name
                     form))
      ((not (numberp form))
       (parse-failed rule form "Not an integer: ~A" form)))
    (parse-success rule form)
    (funcall success rule form)))

(defmethod parse-rule ((rule clgrs::or-node) next-form success)
  ;;(break "Or rule for: ~A" (funcall next-form :peek))
  (let ((index (funcall next-form :get-index))
        (match nil)
        (errors nil)
        (depth *depth*))
    (loop
       :for child-rule :in (clgrs::children rule)
       :while (not match)
       :do
       (handler-case
           (parse-rule child-rule next-form
                       (lambda (r res)
                         (setf match t)
                         (setf (clgrs::node-form rule) res)
                         (parse-success r res)
                         (funcall success r res)))
         (grammar-parse-error (gpe)
           ;; Try next alternative rule
           ;;(break "~A" gpe)
           (push gpe errors)
           (setf *depth* depth)
           (funcall next-form :set-index index)
           
           ;;(break "Next alternative with: ~A" (funcall next-form :peek))
           )
         (clgrs::grammar-marker-found (gmf)
           (if (or *marker-caught*
                   (> (clgrs::depth gmf) (1+ depth)))
               (error gmf)
               (progn
                 (setf *marker-caught* t)
                 (error 'clgrs::grammar-marker-found
                        :marker (clgrs::marker gmf)
                        :rule rule
                        :depth depth))))
         ))
    (when (not match)
      ;; All alternatives failed.
      (setf *depth* depth)
      
      ;; Restore the position we were parsing
      (funcall next-form :set-index index)

      ;; Grab the form at the current position
      (let ((form (funcall next-form :peek)))
        ;; Choose the child error that "parsed more"
        (let ((child-error (first (sort errors '> :key 'depth))))
          (if (>= (depth child-error) (1+ depth))
              ;; If some of the child rules made some progress, then
              ;; return the parsing error from there
              (let ((clgrs/parser::*rule-chain* (cons (clgrs/parser::grammar-rule child-error) clgrs/parser::*rule-chain*)))
                (error child-error))
              ;; else, return the parsing error from here
              (parse-failed rule form
                            (with-output-to-string (s)
                              (format s "Could not parse: ")
                              (if (clgrs::node-name rule)
                                  (format s "~A" (clgrs::node-name rule))
                                  (clgrs/print::print-rule rule s))
                              (write-char #\Newline s)
                              (format s "Invalid form: ~S" form)))))))))

(defmethod parse-rule ((rule clgrs::seq-node) next-form success)
  (let (results)
    (flet ((succ (rule res)
             (push (lambda () (funcall success rule res)) results)
             (setf (clgrs::node-form rule) results)))
      (loop
         :for child-rule :in (clgrs::children rule)
         :do
         (parse-rule child-rule next-form #'succ))
      (parse-success rule (reverse results))
      (dolist (res (reverse results))
        (funcall res)))))

(defmethod parse-rule ((rule clgrs::rule-node) next-form success)
  (let ((r (clgrs::node-rule rule)))
    (setf (clgrs::node-name r) (clgrs::node-name rule))
  (parse-rule r next-form success)))

(defmethod parse-rule ((rule clgrs::any-node) next-form success)
  (let ((form (funcall next-form)))
    (setf (clgrs::node-form rule) form)
    (parse-success rule form)
    (funcall success rule form)))

(defmethod parse-rule ((rule clgrs::optional-node) next-form success)
  (let ((child-rule (clgrs::child rule))
        (index (funcall next-form :get-index))
        (depth *depth*))
    (handler-case
        (parse-rule child-rule next-form
                    (lambda (r res)
                      (setf (clgrs::node-form rule) res)
                      (parse-success r res)
                      (funcall success r res)))
      (grammar-parse-error (gpe)
        ;; Could not parse the optional rule.
        ;; Return with success, but with NIL.
        (setf *depth* depth)
        (funcall next-form :set-index index)
        (parse-success rule nil)
        (funcall success rule nil)))))

(defmethod parse-rule ((rule clgrs::negative-node) next-form success)
  (let ((form (funcall next-form :peek)))
    (handler-case
        (parse-rule (clgrs::child rule) next-form success)
      (grammar-parse-error (gpe)
        (let ((res (form gpe)))
          (parse-success rule res)
          (funcall success rule res)
          (return-from parse-rule res))))
    (parse-failed rule form "Negative parse failed")))

(defmethod parse-rule ((rule clgrs::not-followed-by-node) next-form success)
  (let ((index (funcall next-form :get-index)))
    (flet ((err (rule res)
             (funcall next-form :set-index index)
             (parse-failed rule res "Negative parse failed")))
    (handler-case
        (parse-rule (clgrs::child rule) next-form #'err)
      (grammar-parse-error (gpe)
        (let ((res (form gpe)))
          (funcall next-form :set-index index)
          (when (clgrs::node-action rule)
            (funcall (clgrs::node-action rule) rule res))
          (return-from parse-rule res)))))))

(defmethod parse-rule ((rule clgrs::followed-by-node) next-form success)
  (let ((index (funcall next-form :get-index)))
    (flet ((succ (rule res)
             (funcall next-form :set-index index)
             (when (clgrs::node-action rule)
               (funcall (clgrs::node-action rule) rule res))
             (return-from parse-rule)))
      (parse-rule (clgrs::child rule) next-form #'succ))))

(defun parse-rule* (rule form)
  (let ((result)
        (i 0)
        (*marker-caught* nil))
    (flet ((nxt-form (&optional (action :next) &rest args)
             (case action
               (:next
                (let ((res
                       (if (zerop i)
                           form
                           nil)))
                  (incf i)
                  (incf *depth*)
                  (when (clgrs::grammar-marker-p res)
                    (error 'clgrs::grammar-marker-found
                           :marker res
                           :depth *depth*
                           :rule (first *rule-chain*)))
                  res))
               (:prev ;; TODO
                )
               (:peek
                (if (zerop i)
                    form
                    nil))
               (:get-index
                i)
               (:set-index
                (setf i (car args))))))
      (parse-rule rule
                  #'nxt-form
                  (lambda (rule res)
                    (setf result res)))
      result)))

#+test(progn
        (parse-rule* (clgrs::make-grammar-node '(sym lala)) 'lala)
        (parse-rule* (clgrs::make-grammar-node '(list (sym lala))) '(lala))

        (parse-rule* (clgrs::make-grammar-node '(list (* sym))) '(lala dfdf))

        (parse-rule* (clgrs::make-grammar-node '(list (* sym))) '())

        (parse-rule* (clgrs::make-grammar-node '(list string (* sym))) '("asdf" sdf 2))

        (parse-rule* (clgrs::make-grammar-node '(list (* (or string sym)))) '("sdf" sdf))

        (parse-rule* (clgrs::make-grammar-node '(list string (* (or string sym)))) '("sdf" "sdf" sdf))

        (parse-rule* (clgrs::make-grammar-node '(list (seq (sym :foo) string))) '(:foo "foo"))

        (parse-rule* (clgrs::make-grammar-node `(list (* (or string (seq (sym :foo) (@ string :action ,(lambda (s) (break "~A"s)))))))) '("sdf" "sdf" :foo "sdf"))

        (parse-rule* (clgrs::find-grammar-rule 'clgrs/clos::class-slot) '(asdf :initarg aa :reader asdf))
        )
