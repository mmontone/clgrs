;; https://common-lisp.net/project/asdf/asdf/The-defsystem-grammar.html

(defpackage :clgrs/asdf
  (:use :clgrs :cl))

(in-package :clgrs/asdf)

(defgrammar (asdf:defsystem
                :title "ASDF system"
                :documentation "ASDF defsystem grammar")
    (@ (list (sym asdf:defsystem)
             (@ system-designator :name 'system-designator)
             (@ (* system-option) :name 'system-options))
     :action (lambda (node res)
               (list 'asdf:defsystem :system-designator (second res)
                     :options (cddr res)))
     :documentation "ASDF system"))

(defrule system-designator
    (@ symbol :name 'system-designator))

(defrule operation-name
    (@ symbol :name 'operation-name))

(defrule (system-option :title "ASDF system option")
    (or (@ (seq :defsystem-depends-on system-list)
           :name 'defsystem-depends-on
           :documentation ":defsystem-depends-on allows the programmer to specify another ASDF-defined system or set of systems that must be loaded before the system definition is processed. Typically this is used to load an ASDF extension that is used in the system definition")
        (@ (seq :weakly-depends-on system-list)
           :name 'weakly-depends-on
           :title "Weak dependencies"
           :documentation "The (deprecated) :weakly-depends-on option to defsystem allows the programmer to specify another ASDF-defined system or set of systems that ASDF should try to load, but need not load in order to be successful. Typically this is used if there are a number of systems that, if present, could provide additional functionality, but which are not necessary for basic function.")
        (@ (seq :class system-class-name) :name 'class)
        (@ (seq :build-operation operation-name)
           :name 'operation-name
           :documentation ":build-operation allows the programmer to specify an operation that will be applied, in place of load-op when make (see make) is run on the system. The option value should be the name of an operation. E.g., :build-operation doc-op. This feature is experimental and largely untested. Use at your own risk.")
        (@ (seq :author (@ string :name 'author))
           :name 'author
           :documentation "The system author")
        (@ (seq :version (@ version-specifier :name 'version))
           :name 'version
           :documentation "The system version")
        (@ (seq :description (@ string :name 'description))
           :name 'description
           :documentation "The system description")
        (@ (seq :license (@ string :name 'license))
           :name 'license
           :documentation "The system license")
        (@ (seq :homepage string)
           :name 'homepage
           :documentation "The project home page")
        (seq :bug-tracker string)
        (seq :mailto string)
        (seq :long-name string)
        (seq :source-control source-control)
        (seq :components (@ component-list :name 'component-list))
        (seq :serial (@ boolean :name 'serial))
        ;;(@ module-option :name 'module-option)
        ))

(defrule source-control
    (list keyword string))

(defun available-systems-list ())

(defun make-list-completer (list)
  (lambda (x)
    (find x list)))

(defrule system-class-name
    (@ symbol :title "System class name"
              :validator (clavier:fn (lambda (class-name)
                                       (subtypep class-name 'asdf:system))
                                     "class-name should be a subtype of asdf:system")
              :completer (make-list-completer (available-systems-list))
              :documentation "A system class name will be looked up in the same way as a Component type (see above), except that only system and its subclasses are allowed."))

(defrule simple-component-name
    (or sym str))

(defrule system-list
    (list (* simple-component-name)))

(defrule module-option
    (or
     (seq :components (@ component-list :name 'component-list))
     (seq :serial (@ boolean :name 'serial))))

(defrule component-list
    (list (* (@ component-def :name 'component-def))))

(defrule (component-def :title "Component")
    (list (@ component-type :name 'component-type)
          (@ simple-component-name :name 'component-name)
          (* (@ component-option :name 'component-option))))

(defrule (component-type :title "Component type")
    (or :module
        :file
        :static-file
        sym))

(defrule component-option
    (or (seq :pathname pathname)
        (seq :default-component-class symbol)
        (seq :perform method-form)
        (seq :explain method-form)
        (seq :output-file method-form)
        (seq :if-feature feature-expression)
        (seq :depends-on (list (* dependency-def)))
        (seq :in-order-to (@ (list (+ dependency)) :name 'in-order-to))))

(defrule method-form
    (list operation-name method-qualifier lambda-list &body body))

(defrule method-qualifier
    (member :after :primary :around :before))

(defrule pathname-specifier
    (or pathname string symbol))

(defrule (version-specifier
          :documentation "Version specifiers are strings to be parsed as period-separated lists of integers. Also, the :version argument can be an expression that is resolved to such a string using the following trivial domain-specific language: in addition to being a literal string, it can be an expression of the form (:read-file-form <pathname-or-string> [:at <access-at-specifier]>), or (:read-file-line <pathname-or-string> [:at <access-at-specifier]?>). As the name suggests, the former will be resolved by reading a form in the specified pathname (read as a subpathname of the current system if relative or a unix-namestring), and the latter by reading a line. You may use a uiop:access-at specifier with the :at keyword, by default the specifier is 0, meaning the first form/line is returned. For :read-file-form, subforms can also be specified, with e.g. (1 2 2) specifying “the third subform (index 2) of the third subform (index 2) of the second form (index 1)” in the file (mind the off-by-one error in the English language).

System definers are encouraged to use version identifiers of the form x.y.z for major version, minor version and patch level, where significant API incompatibilities are signaled by an increased major number.")
    (or string
        (@ (list :read-file-form pathname-specifier (? form-specifier))
           :documentation "Resolved by reading a form in the specified pathname (read as a subpathname of the current system if relative or a unix-namestring). Subforms can also be specified, with e.g. (1 2 2) specifying “the third subform (index 2) of the third subform (index 2) of the second form (index 1)” in the file (mind the off-by-one error in the English language)")
        (@
         (list :read-file-line pathname-specifier (? line-specifier))
         :documentation "Resolved by reading a file line")))


(defrule form-specifier
    (seq :at (or integer (list (+ integer)))))

(defrule line-specifier
    (seq :at integer))

(defmacro defsystem (&body def)
  (clgrs/parser::parse-grammar
   (clgrs::find-grammar 'defsystem)
   `(asdf:defsystem ,@def)))
