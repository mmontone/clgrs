;; defstruct grammar
;; http://www.lispworks.com/documentation/lw70/CLHS/Body/m_defstr.htm

(in-package :clgrs)

(defgrammar (defstruct
                :documentation "defstruct defines a structured type, named structure-type, with named slots as specified by the slot-options.")
    (list ($ defstruct)
          struct-name-and-options
          (@ (? (@ string :name 'documentation)) :name 'documentation)
          (@ (* struct-slot-description :name 'slot)
             :name 'slots)))

(defrule struct-slot-description
    (or
     (@ symbol :name 'slot-name)
     (list (@ symbol :name 'slot-name)
           (? (@ any :name 'slot-initform)
              (@ (* struct-slot-option) :name 'slot-options)))))

(defrule slot-option
    (or (seq :type (@ any :name 'slot-type))
        (seq :read-only (@ boolean :name 'read-only))))

(defrule structure-name
    (@ symbol :name 'structure-name))

(defrule struct-name-and-options
    (or structure-name
        (list structure-name structure-options)))

(defrule structure-options
    (or (@ struct-conc-name-option :name 'conc-name)
        (@ struct-constructor-option :name 'constructor
                                     :documentation "Specifies the function to construct the structure")
        (@ struct-copier-option :name 'copier)
        (@ struct-include-option :name 'include)
        (@ struct-named-option :name 'named)
        (@ struct-predicate-option :name 'predicate)
        (@ struct-printer-option :name 'printer-option)
        (@ struct-type-option :name 'type-option)))

(defrule struct-conc-name-option
    (list :conc-name (@ symbol :name 'conc-name)))

(defrule struct-constructor-option
    (list :constructor (@ symbol :name 'constructor-name)))

(defrule struct-copier-option
    (list :copier (@ symbol :name 'copier-name)))

(defrule struct-include-option
    (list :include (@ symbol :name 'included-structure-name)
          (@ (* struct-slot-description) :name 'slot-descriptions)))

(defrule struct-named-option
    :named)

(defrule struct-predicate-option
    (list :predicate (@ symbol :name 'predicate-name)))

(defrule struct-printer-option
    (or
     (list :print-object (@ symbol :name 'printer-name))
     (list :print-function (@ symbol :name 'printer-name))))

(defrule struct-type-option
    (list :type (@ any :name 'type)))
