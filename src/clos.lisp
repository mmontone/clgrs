;; http://clhs.lisp.se/Body/m_defcla.htm

(defpackage :clgrs/clos
  (:use :clgrs :cl))

(in-package :clgrs/clos)

(defgrammar (cl:defclass :documentation "Define a CLOS class")
  (list (sym cl:defclass) (@ symbol :name 'class-name)
        class-superclasses
        class-slots
        class-options))

(defrule class-superclasses
    (@ (list (* (@ symbol :name 'super-class)))
           :name 'super-classes))

(defrule class-slots
    (@ (list (* class-slot)) :name 'class-slots
       :format '(:before (:newline :indent))))

(defrule class-slot
    (@ (list (@ symbol :name 'slot-name)
             (* (@ class-slot-option :name 'slot-option
                                     :format '(:after :newline))))
       :name 'class-slot
       :documentation "A class slot"
       :format '(:after :newline)))

(defrule class-slot-option
    (or (@ (seq :reader (@ symbol :name 'reader))
           :documentation "The :reader slot option specifies that an unqualified method is to be defined on the generic function named reader-function-name to read the value of the given slot. ")
        (@ (seq :writer (@ symbol :name 'writer))
           :documentation "The :writer slot option specifies that an unqualified method is to be defined on the generic function named writer-function-name to write the value of the slot.")
        (@ (seq :accessor (@ symbol :name 'accessor))
           :documentation "The :accessor slot option specifies that an unqualified method is to be defined on the generic function named reader-function-name to read the value of the given slot and that an unqualified method is to be defined on the generic function named (setf reader-function-name) to be used with setf to modify the value of the slot. ")
        (@ (seq :allocation (@ (or (@ :instance
                                      :documentation "If allocation-type is :instance, a local slot of the name slot-name is allocated in each instance of the class. ")
                                   (@ :class :documentation " If allocation-type is :class, a shared slot of the given name is allocated in the class object created by this defclass form. The value of the slot is shared by all instances of the class. If a class C1 defines such a shared slot, any subclass C2 of C1 will share this single slot unless the defclass form for C2 specifies a slot of the same name or there is a superclass of C2 that precedes C1 in the class precedence list of C2 and that defines a slot of the same name. ")) :name 'allocation-type))
           :documentation " The :allocation slot option is used to specify where storage is to be allocated for the given slot. Storage for a slot can be located in each instance or in the class object itself. The value of the allocation-type argument can be either the keyword :instance or the keyword :class. If the :allocation slot option is not specified, the effect is the same as specifying :allocation :instance.")
        (@ (seq :initarg (@ #+nil(symbol) keyword :name 'initarg))
           :documentation "The :initarg slot option declares an initialization argument named initarg-name and specifies that this initialization argument initializes the given slot. If the initialization argument has a value in the call to initialize-instance, the value will be stored into the given slot, and the slot's :initform slot option, if any, is not evaluated. If none of the initialization arguments specified for a given slot has a value, the slot is initialized according to the :initform slot option, if specified.")
        (@ (seq :initform (@ any :name 'initform))
           :documentation "The :initform slot option is used to provide a default initial value form to be used in the initialization of the slot. This form is evaluated every time it is used to initialize the slot. The lexical environment in which this form is evaluated is the lexical environment in which the defclass form was evaluated. Note that the lexical environment refers both to variables and to functions. For local slots, the dynamic environment is the dynamic environment in which make-instance is called; for shared slots, the dynamic environment is the dynamic environment in which the defclass form was evaluated.")
        (@ (seq :documentation (@ string :name 'documentation))
           :documentation "The :documentation slot option provides a documentation string for the slot.")
        (@ (seq :type (@ any :name 'slot-type))
           :documentation "The :type slot option specifies that the contents of the slot will always be of the specified data type. It effectively declares the result type of the reader generic function when applied to an object of this class.")))

(defrule class-options
    (@ (* class-option) :name 'class-options
       :format '(:before :newline)))

(defrule class-option
    (@ (or
        (list :default-initargs (@ (* any) :name 'default-initargs))
        (list :documentation (@ string :name 'documentation))
        (list :metaclass (@ symbol :name 'metaclass)))
     :format '(:after :newline :before :newline)))

(defmacro defclass* (&body def)
  (clgrs/parser::parse-grammar
   (clgrs::find-grammar 'cl:defclass)
   `(cl:defclass ,@def)))

(with-output-to-string (s)
  (clgrs/print::print-rule (clgrs::grammar (clgrs::find-grammar 'cl:defclass)) s))

(with-output-to-string (s)
  (clgrs/print::print-rule (clgrs::find-grammar-rule 'class-slot) s))

(with-output-to-string (s)
  (clgrs/print::print-rule (clgrs::find-grammar-rule 'class-slot-option) s))

(clgrs/parser::parse-grammar
 (clgrs::find-grammar 'cl:defclass)
 '(defclass lala () ((a :initarg aa))
   (:documentation "lala")))
