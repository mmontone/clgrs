;; clgrs.el
;;
;; Common Lisp Grammar Services minor-mode

(require 'yasnippet)
(require 'slime)
(require 'popup)
(require 'cl)

(defun clgrs/sexp-at-point ()
  (slime-sexp-at-point))

(defun clgrs/goto-toplevel-sexp ()
  (slime-beginning-of-defun))

(defun clgrs/goto-toplevel-end ()
  (slime-end-of-defun))

(defun clgrs/toplevel-form ()
  ;;(slime-defun-at-point)

  (save-excursion
    (slime-beginning-of-defun)
    (slime-sexp-at-point))
  )

(defun clgrs/debug-marked-form ()
  ;; (slime-eval-async `(swank:eval-and-grab-output ,(concat "(quote "(clgrs/marked-form) ")"))
  ;;   (lambda (result)
  ;;     (cl-destructuring-bind (output value) result
  ;;       (let ((string (concat output value)))
  ;;         (return string)))))
  (let ((expr (concat "(quote " (clgrs/marked-form) ")")))
    (debug expr)
    (slime-eval-async `(swank:eval-and-grab-output ,expr)
      (lambda (result)
        (cl-destructuring-bind (output value) result
          (return value))))))

(defun clgrs/process-assist-item (item)
  (when item
    (ecase (getf item :ACTION)
      (:ENTER (yas-expand-snippet (getf item :SNIPPET)))
      (:APPEND (yas-expand-snippet (getf item :SNIPPET))))))


(cl-defun clgrs/completing-read-emacs (options &key prompt)
  (completing-read (or prompt "Select:")
                   (mapcar (lambda (opt)
                             (getf opt :MESSAGE))
                           options)))

(cl-defun clgrs/completing-read-ido (options &key prompt)
  (ido-completing-read (or prompt "Select:")
                       (mapcar (lambda (opt)
                                 (getf opt :MESSAGE))
                               options)))

(cl-defun clgrs/completing-read-popup (options &key prompt)
  (popup-menu* (mapcar
                (lambda (opt)
                  (popup-make-item (getf opt :MESSAGE)
                                   :document (getf opt :DOCUMENTATION)
                                   :summary (and (not (null (getf opt :TYPE)))
                                                 (not (equalp (getf opt :TYPE) 'NIL))
                                                 (prin1-to-string (getf opt :TYPE)))
                                   :value (getf opt :MESSAGE)))
                options)
               :isearch t
               ;; help-delay enables quick-help
               :help-delay 0.5))

(defvar clgrs/completing-read-function 'clgrs/completing-read-popup)

(cl-defun clgrs/read-completion (options &key prompt)
  (funcall clgrs/completing-read-function options :prompt prompt))

(defun clgrs/assist-at-point-async ()
  (interactive)
  (let ((expr (concat "(clgrs/assist::emacs-assist-form (quote "
                      (clgrs/marked-form)
                      "))")))
    (slime-eval-async `(swank:eval-and-grab-output ,expr)
      (lambda (result)
        (with-local-quit
          (cl-destructuring-bind (output value) result
            ;;(debug (first (read-from-string value)))
            (when (not (equal value ":ERROR"))
              (let ((options (first (read-from-string value))))
                (if (= (length options) 1)
                    (clgrs/process-assist-item (first options))
                  ;; else

                  (let ((completion (clgrs/read-completion
                                     (mapcar (lambda (item)
                                               ;;(getf item :MESSAGE)
                                               item)
                                             options))))
                    ;; completing-read returns only the completed string. argh.
                    (let ((selection (seq-find (lambda (item)
                                                 (string= (getf item :MESSAGE) completion))
                                               options)))
                      ;;(debug selection)
                      (clgrs/process-assist-item selection))))))))))))

(defun clgrs/assist-at-point ()
  (interactive)
  (let ((expr (concat "(clgrs/assist::emacs-assist-form (quote "
                      (clgrs/marked-form)
                      "))")))
    (let ((result (slime-eval `(swank:eval-and-grab-output ,expr))))
      (with-local-quit
        (cl-destructuring-bind (output value) result
          ;;(debug (first (read-from-string value)))
          (when (not (equal value ":ERROR"))
            (let ((options (first (read-from-string value))))
              (if (= (length options) 1)
                  (clgrs/process-assist-item (first options))
                ;; else

                (let ((completion (clgrs/read-completion
                                   (mapcar (lambda (item)
                                             ;;(getf item :MESSAGE)
                                             item)
                                           options))))
                  ;; completing-read returns only the completed string. argh.
                  (let ((selection (seq-find (lambda (item)
                                               (string= (getf item :MESSAGE) completion))
                                             options)))
                    ;;(debug selection)
                    (clgrs/process-assist-item selection))))))))))
  nil)

(defun clgrs/assist-available ()
  (let ((marked-form (clgrs/marked-form)))
    (when marked-form
      (let ((expr (concat "(clgrs/assist::emacs-assist-form (quote "
                          marked-form
                          "))")))
        (let ((result (slime-eval `(swank:eval-and-grab-output ,expr))))
          (with-local-quit
            (cl-destructuring-bind (output value) result
              (not (equalp value ":ERROR")))))))))

(defun clgrs/expand-grammar (grammar)
  (yas-expand-snippet (getf grammar :SNIPPET)))

(defun clgrs/insert-grammar ()
  (interactive)
  (let ((expr "(clgrs/assist::emacs-list-grammars)"))
    (slime-eval-async `(swank:eval-and-grab-output ,expr)
      (lambda (result)
        (with-local-quit
          (cl-destructuring-bind (output value) result
            (let ((grammars (first (read-from-string value))))
              (let ((completion (clgrs/read-completion
                                 (mapcar (lambda (item)
                                           ;;(getf item :MESSAGE)
                                           item
                                           )
                                         grammars)
                                 :prompt "Grammar:")))
                ;; completing-read returns only the completed string. argh.
                (let ((grammar (seq-find (lambda (item)
                                           (string= (getf item :MESSAGE) completion))
                                         grammars)))
                  (clgrs/expand-grammar grammar))))))))))

(cl-defun clgrs/marked-form ()
  (let ((current-pos (point))
        (toplevel-pos (save-excursion
                        (clgrs/goto-toplevel-sexp)
                        (point)))
        (toplevel-form (clgrs/toplevel-form))
        (content))
    (when (not toplevel-form)
      (return-from clgrs/marked-form nil))
    (save-excursion
      (with-temp-buffer
        (erase-buffer)
        (goto-char 0)
        (insert toplevel-form)
        (goto-char (- current-pos toplevel-pos -1))
        ;; Insert the mark
        (let ((sexp-at-point (slime-sexp-at-point)))
          (if sexp-at-point
              (let ((bounds (slime-bounds-of-sexp-at-point)))
                (goto-char (cdr bounds))
                (insert "))")
                (goto-char (- (car bounds) 1))
                (insert " #.(clgrs/assist::make-assist-marker :form (quote "))
            (insert "#.(clgrs/assist::make-assist-marker) ")))
        (setq content (buffer-string))))
    content))

(defun yas-clear-current-field ()
  (yas--skip-and-clear (yas-current-field)))

;; Yasnippet patch. Ignore errors on this function.
;; Use defadvice instead?

(defun yas--advance-end-maybe (fom newend)
  "Maybe advance FOM's end to NEWEND if it needs it.

If it does, also:

* call `yas--advance-start-maybe' on FOM's next fom.

* in case FOM is field call `yas--advance-end-maybe' on its parent
  field

Also, if FOM is an exit-marker, always call
`yas--advance-start-maybe' on its next fom.  This is because
exit-marker have identical start and end markers."
  (condition-case nil
      (cond ((and fom (< (yas--fom-end fom) newend))
             (set-marker (yas--fom-end fom) newend)
             (yas--advance-start-maybe (yas--fom-next fom) newend)
             (yas--advance-end-of-parents-maybe (yas--fom-parent-field fom) newend))
            ((yas--exit-p fom)
             (yas--advance-start-maybe (yas--fom-next fom) newend)))
    (error nil)))


;; Context help

(defun rgr/toggle-context-help ()
  "Turn on or off the context help.
Note that if ON and you hide the help buffer then you need to
manually reshow it. A double toggle will make it reappear"
  (interactive)
  (with-current-buffer (help-buffer)
    (unless (local-variable-p 'context-help)
      (set (make-local-variable 'context-help) t))
    (if (setq context-help (not context-help))
        (progn
          (if (not (get-buffer-window (help-buffer)))
              (display-buffer (help-buffer)))))
    (message "Context help %s" (if context-help "ON" "OFF"))))

(defun rgr/context-help ()
  "Display function or variable at point in *Help* buffer if visible.
Default behaviour can be turned off by setting the buffer local
context-help to false"
  (interactive)
  (let ((rgr-symbol (symbol-at-point))) ; symbol-at-point http://www.emacswiki.org/cgi-bin/wiki/thingatpt%2B.el
    (with-current-buffer (help-buffer)
      (unless (local-variable-p 'context-help)
        (set (make-local-variable 'context-help) t))
      (if (and context-help (get-buffer-window (help-buffer))
               rgr-symbol)
          (if (fboundp  rgr-symbol)
              (describe-function rgr-symbol)
            (if (boundp  rgr-symbol) (describe-variable rgr-symbol)))))))

(defadvice eldoc-print-current-symbol-info
    (around eldoc-show-c-tag activate)
  (cond
   ((eq major-mode 'emacs-lisp-mode) (rgr/context-help) ad-do-it)
   ((eq major-mode 'lisp-interaction-mode) (rgr/context-help) ad-do-it)
   ((eq major-mode 'apropos-mode) (rgr/context-help) ad-do-it)
   (t ad-do-it)))

(global-set-key (kbd "C-c h") 'rgr/toggle-context-help)

(setf eldoc-echo-area-use-multiline-p t)

(define-minor-mode my-contextual-help-mode
  "Displays help for the current symbol whenever the *Help* buffer is visible.

Advises `eldoc-print-current-symbol-info'."
  :lighter " C-h"
  :global t
  (require 'help-mode) ;; for `help-xref-interned'
  (message "Contextual help is %s" (if my-contextual-help-mode "on" "off"))
  (and my-contextual-help-mode
       (eldoc-mode 1)
       (eldoc-current-symbol)
       (my-contextual-help :force)))

(defadvice eldoc-print-current-symbol-info (before my-contextual-help activate)
  "Triggers contextual elisp *Help*. Enabled by `my-contextual-help-mode'."
  (and my-contextual-help-mode
       (derived-mode-p 'emacs-lisp-mode)
       (my-contextual-help)))

(defun my-contextual-help (&optional force)
  "Display function or variable at point in *Help* buffer, if visible."
  (when (or force (get-buffer-window (help-buffer)))
    (let ((sym (eldoc-current-symbol)))
      ;; If something else changes the help buffer contents, ensure we
      ;; don't immediately revert back to the current symbol's help.
      (and sym
           (not (keywordp sym))
           (not (eq sym (get 'my-contextual-help 'last-sym)))
           (put 'my-contextual-help 'last-sym sym)
           (save-selected-window
             (help-xref-interned sym))))))

(my-contextual-help-mode 1)


(defun describe-function-in-popup ()
  (interactive)
  (let* ((thing (symbol-at-point))
         (description (save-window-excursion
                        (describe-function thing)
                        (switch-to-buffer "*Help*")
                        (buffer-string))))
    (popup-tip description
               :point (point)
               :around t
               :height 30
               :scroll-bar t
               :margin t)))

(global-set-key (kbd "C-c h") 'describe-function-in-popup)

(defun clgrs/yas-optional ()
  (if (and yas-moving-away-p (not yas-modified-p))
      ""
    yas-text))

(yas-expand-snippet "${1:lala}
${2:[foo]$(clgrs/yas-optional)}
${3:bar}")

(defun clgrs/yas-popup (list)
  (if (and (not yas-moving-away-p) (not yas-modified-p))
      (symbol-name (popup-menu* list) :isearch t)
    yas-text))

(yas-expand-snippet "${1:lala} ${2:complete$(clgrs/yas-popup '(foo bar baz))}")

(defun clgrs/optional-fn (fn)
  (if (and yas-moving-away-p (not yas-modified-p))
      (funcall fn)
    (yas-expand-snippet snippet)))

(yas-expand-snippet "${1:lala} ${2:clgrs/optional-fn(\")}")

(defvar *clgrs/assisting* nil)

(defun clgrs/yas-complete (func-name)
  (if (and ;;(not *clgrs/assisting*)
       (not yas-moving-away-p)
       (not yas-modified-p))
      (progn
        (when (not (zerop (length yas-text)))
          (delete-region (point) (+ (point) (length yas-text))))
        (clgrs/complete func-name))))

(defun clgrs/complete-func (func-name)
  (let ((expr (concat "(" func-name ")")))
    (let ((result (slime-eval `(swank:eval-and-grab-output ,expr))))
      (with-local-quit
        (cl-destructuring-bind (output value) result
          ;;(debug (first (read-from-string value)))
          (let ((options (first (read-from-string value))))
            (if (= (length options) 1)
                (clgrs/process-assist-item (first options))
              ;; else

              (let ((completion (clgrs/read-completion
                                 (mapcar (lambda (item)
                                           ;;(getf item :MESSAGE)
                                           item)
                                         options))))
                ;; completing-read returns only the completed string. argh.
                (let ((selection (seq-find (lambda (item)
                                             (string= (getf item :MESSAGE) completion))
                                           options)))
                  (getf selection :VALUE))))))))))

(defun clgrs/yas-assist ()
  (if (and (not *clgrs/assisting*)
           (not yas-moving-away-p)
           (not yas-modified-p))
      (let ((*clgrs/assisting* t))
        (clgrs/assist-at-point)
        yas-text)
    yas-text))

(defun clgrs/yas-assist-placeholder ()
  (if (and ;;(not *clgrs/assisting*)
       (not yas-moving-away-p)
       (not yas-modified-p))
      (let ((*clgrs/assisting* t))
        ;; Delete the place holder before calling assist-at-point
        (when (not (zerop (length yas-text)))
          (delete-region (point) (+ (point) (length yas-text))))
        ;; We need this so that the completion is written into snippet space
        ;;(when field
        ;;  (goto-char (yas--field-start field)))
        ;; Assist at cursor pointer
        (clgrs/assist-at-point)
        " ")
    yas-text))

(defun company-complete-wrapper (func &rest args)
  (if (zerop (current-column))
      (clgrs/insert-grammar)
    (if (clgrs/assist-available)
        (clgrs/assist-at-point)
      (apply func args))))

;;;###autoload
(define-minor-mode clgrs-mode
  "Common Lisp Grammar Services minor mode"
  :lighter " clgrs"
  :keymap (let ((map (make-sparse-keymap)))
            ;;(define-key map (kbd "C-1") 'clgrs/insert-grammar)
            ;;(define-key map (kbd "C-2") 'clgrs/assist-at-point)
            map)
  (advice-add 'company-complete :around #'company-complete-wrapper))

;;;###autoload
(add-hook 'lisp-mode-hook 'clgrs-mode)

(provide 'clgrs)
