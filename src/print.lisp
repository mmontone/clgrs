(defpackage :clgrs/print
  (:use :clgrs :cl))

(in-package :clgrs/print)

(defun print-grammar (grammar &optional (stream *standard-output*))
  )

(defgeneric print-rule (rule stream))

(defmethod print-rule (node stream)
  (format stream "~A" (type-of node)))

(defmethod print-rule ((node clgrs::symbol-node) stream)
  (cond
    ((and (clgrs::node-name node)
          (clgrs::node-symbol node))
     (format stream "~S[~S]"
             (clgrs::node-symbol node)
             (clgrs::node-name node)))
    ((clgrs::node-name node)
     (format stream "~S"
             (clgrs::node-name node)))
    ((clgrs::node-symbol node)
     (format stream "~S" (clgrs::node-symbol node)))
    (t (format stream "symbol"))))

(print-rule (clgrs::make-grammar-node 'symbol) nil)
(print-rule (clgrs::make-grammar-node '(sym lala)) nil)
(print-rule (clgrs::make-grammar-node '(@ sym :name name)) nil)

(defmethod print-rule ((node clgrs::string-node) stream)
  (cond
    ((and (clgrs::node-name node)
          (clgrs::node-string node))
     (format stream "~S::string=~S"
             (clgrs::node-name node)
             (clgrs::node-string node)))
    ((clgrs::node-name node)
     (format stream "~S::string"
             (clgrs::node-name node)))
    ((clgrs::node-string node)
     (format stream "string=~S" (clgrs::node-string node)))
    (t (format stream "string"))))

(print-rule (clgrs::make-grammar-node 'string) nil)
(print-rule (clgrs::make-grammar-node '(@ string :name name)) nil)

(defmethod print-rule ((node clgrs::list-node) stream)
  (write-string "( " stream)
  (dolist (child (clgrs::children node))
    (print-rule child stream)
    (write-char #\space stream))
  (write-string " )" stream))

(with-output-to-string (s)
  (print-rule (clgrs::make-grammar-node '(list string symbol)) s))

(defmethod print-rule ((node clgrs::seq-node) stream)
  (dolist (child (clgrs::children node))
    (print-rule child stream)
    (write-char #\space stream)))

(defmethod print-rule ((node clgrs::or-node) stream)
  (print-rule (first (clgrs::children node)) stream)
  (dolist (child (rest (clgrs::children node)))
    (write-string " | " stream)
    ;; Consider pretty printing!!
    (write-char #\Newline stream)
    (print-rule child stream)))

(with-output-to-string (s)
  (print-rule (clgrs::make-grammar-node '(or (list string symbol)
                                            (@ string :name foo)))
              s))

(defmethod print-rule ((node clgrs::many-node) stream)
  (print-rule (clgrs::child node) stream)
  (write-string "*" stream))

(defmethod print-rule ((node clgrs::optional-node) stream)
  (print-rule (clgrs::child node) stream)
  (write-string "?" stream))

(defmethod print-rule ((node clgrs::many-plus-node) stream)
  (print-rule (clgrs::child node) stream)
  (write-string "+" stream))

(defmethod print-rule ((node clgrs::rule-node) stream)
  (when (clgrs::node-name node)
    (format stream "~S::" (clgrs::node-name node)))
  (format stream "<~A>" (clgrs::node-rule node)))

(defmethod print-rule ((node clgrs::member-node) stream)
  (when (clgrs::node-name node)
    (format stream "~S::" (clgrs::node-name node)))
  (format stream "(member ~S)" (clgrs::node-members node)))

(defmethod print-rule ((node clgrs::any-node) stream)
  (if (clgrs::node-name node)
      (format stream "~A::ANY" (clgrs::node-name node))
      (format stream "ANY")))

(defmethod print-rule ((node clgrs::followed-by-node) stream)
  (when (clgrs::node-name node)
    (format stream "~S::" (clgrs::node-name node)))
  (write-string "&" stream)
  (print-rule (clgrs::child node) stream))

(defmethod print-rule ((node clgrs::not-followed-by-node) stream)
  (when (clgrs::node-name node)
    (format stream "~S::" (clgrs::node-name node)))
  (write-string "!" stream)
  (print-rule (clgrs::child node) stream))

;; Nodes printing

(defmethod print-object ((node clgrs::node) stream)
  (print-unreadable-object (node stream :type t :identity t)
    (print-rule node stream)))
