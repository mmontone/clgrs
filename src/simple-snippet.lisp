(in-package :clgrs/assist)

(defvar *indentation* 0)
(defvar *indent-size* 4)
(defvar *in-newline* t)

(defun indent-node-before (node stream)
  (apply-node-format node (getf (clgrs::node-format node) :before) stream)
  (when *in-newline*
    (dotimes (i *indentation*)
      (write-char #\space stream))))

(defun indent-node-after (node stream)
  (apply-node-format node (getf (clgrs::node-format node) :after) stream))

(defun apply-node-format (node format stream)
  (apply-node-format-rule format node stream))

(defmethod apply-node-format-rule ((format list) node stream)
  (mapcar (lambda (rule)
            (apply-node-format-rule rule node stream))
          format))

(defmethod apply-node-format-rule ((format (eql :newline)) node stream)
  (write-char #\newline stream)
  (setf *in-newline* t))

(defmethod apply-node-format-rule ((format (eql :indent)) node stream)
  (incf *indentation* *indent-size*))

(defmethod apply-node-format-rule ((format (eql :unindent)) node stream)
  (decf *indentation* *indent-size*))

(defun simple-snippet* (node)
  (let ((*snippet-n* 0)
        (*print-case* :downcase)
        (*indentation* 0)
        (*in-newline* nil))
    (with-output-to-string (out)
      (simple-snippet node out))))

(defgeneric simple-snippet (node stream))

(defmethod simple-snippet :before ((node clgrs::node) stream)
  (indent-node-before node stream))

(defmethod simple-snippet :after ((node clgrs::node) stream)
  (setf *in-newline* nil)
  (indent-node-after node stream))

(defmethod simple-snippet ((node clgrs::symbol-node) stream)
  (if (clgrs::node-symbol node)
      (prin1 (clgrs::node-symbol node) stream)
      (format stream "${~A:~A~@[~A~]}"
              (next-snippet-n)
              (or
               (clgrs::node-name node)
               'symbol)
              (when (clgrs::node-completer node)
                (format nil "$(clgrs/complete-func \"~S\")"
                        (clgrs::node-completer node))
                ))))

(defmethod simple-snippet ((node clgrs::keyword-node) stream)
  (if (clgrs::node-keyword node)
      (prin1 (clgrs::node-keyword node) stream)
      (format stream ":${~A:~A~@[~A~]}"
              (next-snippet-n)
              (or (clgrs::node-name node)
                  'keyword)
              (when (clgrs::node-completer node)
                (format nil "$(clgrs/complete-func \"~S\")"
                        (clgrs::node-completer node))
                ))))

(defmethod simple-snippet ((node clgrs::string-node) stream)
  (if (clgrs::node-string node)
      (prin1 (clgrs::node-string node) stream)
      (format stream "\"${~A:~A}\""
              (next-snippet-n)
              (or (clgrs::node-name node)
                  "string"))))

(defmethod simple-snippet ((node clgrs::integer-node) stream)
  (format stream "${~A:~S}"
          (next-snippet-n)
          (or (clgrs::node-integer node)
              (clgrs::node-name node)
              'integer)))

(defmethod simple-snippet ((node clgrs::boolean-node) stream)
  (format stream "~A"
          (or (clgrs::node-boolean node)
              (clgrs::node-name node)
              'boolean)))

(defun yas-assist (stream &optional (placeholder "$"))
  (format stream "${~A:~A$(clgrs/yas-assist-placeholder)}"
          (next-snippet-n)
          (or placeholder "$")))


(defmethod simple-snippet ((node clgrs::list-node) stream)
  (write-string "(" stream)

  (let ((children (clgrs::children node)))
    (when (first children)
      (simple-snippet (first children) stream))
    (loop
       for child in (cdr children)
       do
         (write-char #\space stream)
         (simple-snippet child stream)))

  (write-string ")" stream))

(defmethod simple-snippet ((node clgrs::or-node) stream)
  (yas-assist stream (clgrs::node-name node)))

(defmethod simple-snippet ((node clgrs::rule-node) stream)
  (if (and (clgrs::node-name node) nil)
      (format stream "${~A:~A}"
              (next-snippet-n)
              (clgrs::node-name node))
      (simple-snippet (clgrs::node-rule node) stream)))

(defmethod simple-snippet ((node clgrs::many-node) stream)
  (yas-assist stream (clgrs::node-name node)))

(defmethod simple-snippet ((node clgrs::seq-node) stream)
  (if (and nil (clgrs::node-name node))
      (yas-assist stream (clgrs::node-name node))
      (progn
        (when (first (clgrs::children node))
          (simple-snippet (first (clgrs::children node)) stream))
        (loop for child in (cdr (clgrs::children node))
           do
             (write-char #\space stream)
             (simple-snippet child stream)))))

(defmethod simple-snippet ((node clgrs::any-node) stream)
  (if (clgrs::node-name node)
      (format stream "${~A:~A}" (next-snippet-n)
              (clgrs::node-name node))
      (format stream "$~A" (next-snippet-n))))      

(defmethod simple-snippet ((node clgrs::optional-node) stream)
  (format stream "${~A:[~A]$(clgrs/yas-optional)}"
          (next-snippet-n)
          (with-output-to-string (s)
            (assist-template (clgrs::child node) s))))
