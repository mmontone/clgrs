(defpackage :clgrs.cl-forms
  (:use :clgrs :cl))

(in-package :clgrs.cl-forms)

(defgrammar (forms:defform
                :documentation "Define a form")
    (list (sym forms:defform) (@ sym :name 'form-name)
          (@ form-options :name 'form-options)
          (@ form-fields :name 'form-fields)))

(defrule (form-options :name 'form-options)
    (list (* form-option)))

(defrule form-option
    (or (@ (seq :action (@ string :name 'action))
           :name 'action
           :documentation (clgrs::slot-doc 'forms::action 'forms::form))
        (@ (seq :method (@ (or :get :post) :name 'method))
           :name 'method
           :documentation (clgrs::slot-doc 'forms::method 'forms::form))
        (@ (seq :enctype (@ string :name 'enctype))
           :name 'enctype
           :documentation (clgrs::slot-doc 'forms::enctype 'forms::form))
        (@ (seq :client-validation (@ boolean :name 'client-validation))
           :name 'client-validation
           :documentation (clgrs::slot-doc 'forms::client-validation 'forms::form))
        (@ (seq :csrf-protection (@ boolean :name 'csrf-protection))
           :name 'csrf-protection
           :documentation (clgrs::slot-doc 'forms::csrf-protection 'forms::form))))

(defrule form-fields
    (list (* form-field)))

(defun field-type-completer ()
  (let ((*print-case* :downcase))
    (list (list :message ":string" :value (prin1-to-string 'string)
                :documentation "String field")
          (list :message ":integer" :value (prin1-to-string 'integer)
                :documentation "Integer field")
          (list :message ":boolean"
                :value (prin1-to-string 'boolean)
                :documentation "Boolean field"))))

(defrule form-field
    (list (@ symbol :name 'field-name)
          (@ keyword :name 'field-type ;;:bind 'field-type
                     :completer 'field-type-completer)
          ;; Dynamic rule here
          ;; A dynamic rule is a function that returns a rule
          ;;(@ (dyn field-attributes) :name 'field-attributes)
          field-attributes))

;; Dynamic field attributes
(defun field-attributes (bindings)
  (let ((field-type (getf bindings 'field-type)))
    (loop
       for (initarg . slot-type) in (get-field-type-attributes field-type)
       collect (clgrs::make-grammar-node `(@ (seq ,initarg (@ ,slot-type :name ',initarg)))))))

;; Static field attributes
(defrule field-attributes
    (* field-attribute))

(defrule field-attribute
    (or (@ (seq :label (@ string :name 'field-label))
           :name 'field-label
           :documentation (clgrs::slot-doc 'forms::label 'forms::form-field))
        (@ (seq :help-text (@ string :name 'help-text))
           :name 'help-text
           :documentation (clgrs::slot-doc 'forms::help-text 'forms::form-field))
        (@ (seq :required-p (@ boolean :name 'required-p))
           :name 'required-p
           :documentation (clgrs::slot-doc 'forms::required 'forms::form-field))
        (@ (seq :constraints (@ any :name 'constraints))
           :name 'constraints
           :documentation (clgrs::slot-doc 'forms::constraints 'forms::form-field))
        (@ (seq :reader (@ any :name 'field-reader))
           :name 'field-reader
           :documentation (clgrs::slot-doc 'forms::reader 'forms::form-field))
        (@ (seq :writer (@ any :name 'field-writer))
           :name 'field-writer
           :documentation (clgrs::slot-doc 'forms::writer 'forms::form-field))
        (@ (seq :accessor (@ any :name 'field-accessor))
           :name 'field-accessor
           :documentation (clgrs::slot-doc 'forms::accessor 'forms::form-field))
        ))
