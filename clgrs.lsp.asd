(asdf:defsystem #:clgrs.lsp
  :description "Language Service Protocoal for Common Lisp Grammar Services"
  :author "Mariano Montone <marianomontone@gmail.com>"
  :license  "MIT"
  :version "0.0.1"
  :serial t
  :components ((:module "lsp"
                        :components
                        ((:file "package")
                         (:file "lsp"))))
  :depends-on (:clgrs :cl-lsp))
